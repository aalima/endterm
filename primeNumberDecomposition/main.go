package main

import (
	"endterm/PrimeNumberDecomposition/pb"
	"google.golang.org/grpc"
	"io"
	"log"
	"net"
	"time"
)

type Server struct {
	pb.UnimplementedSumServiceServer
}

func (s *Server) PrimeDecomposition(req *pb.PrimeDecompositionRequest, stream pb.SumService_PrimeDecompositionServer) error{
	n := req.GetNumber().GetNumber()
	k := int32(2)
	for n > 1 {
		if n % k == 0 {
			res := &pb.PrimeDecompositionResponse{Result: k}
			n = n/k
			if err := stream.Send(res); err != nil {
				log.Fatalf("error while sending PrimeDecomposition requests: %v", err.Error())
			}
			time.Sleep(time.Second)
		} else {
			k = k + 1
		}
	}
	return nil
}

func (s *Server) ComputeAverage(stream pb.SumService_ComputeAverageServer) error {
	var result int32 = 0
	var i float64 = 0
	for {
		req, err := stream.Recv()
		if err == io.EOF {
			return stream.SendAndClose(&pb.ComputeAverageResponse{
				Result: float64(result)/i,
			})
		}
		if err != nil {
			log.Fatalf("Error while reading client stream: %v", err)
		}
		i += 1
		result += req.Number.GetNumber()
	}
}


func main() {
	l, err := net.Listen("tcp", "0.0.0.0:50005")
	if err != nil {
		log.Fatalf("Failed to listen:%v", err)
	}
	s := grpc.NewServer()
	pb.RegisterSumServiceServer(s, &Server{})
	log.Println("Server is running on port:50005")
	if err := s.Serve(l); err != nil {
		log.Fatalf("failed to serve:%v", err)
	}
}
