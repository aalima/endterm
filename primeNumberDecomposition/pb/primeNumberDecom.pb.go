package pb

import (
	context "context"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
protoimpl "google.golang.org/protobuf/runtime/protoimpl"
reflect "reflect"
sync "sync"
)

const (
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type Number struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields
	Number int32 `protobuf:"varint,1,opt,name=number,proto3" json:"number,omitempty"`
}

func (x *Number) Reset() {
	*x = Number{}
	if protoimpl.UnsafeEnabled {
		mi := &file_proto_primepb_prime_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Number) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Number) ProtoMessage() {}

func (x *Number) ProtoReflect() protoreflect.Message {
	mi := &file_proto_primepb_prime_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

func (*Number) Descriptor() ([]byte, []int) {
	return file_proto_primepb_prime_proto_rawDescGZIP(), []int{0}
}

func (x *Number) GetNumber() int32 {
	if x != nil {
		return x.Number
	}
	return 0
}

type PrimeDecompositionRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields
	Number *Number `protobuf:"bytes,1,opt,name=number,proto3" json:"number,omitempty"`
}

func (x *PrimeDecompositionRequest) Reset() {
	*x = PrimeDecompositionRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_proto_primepb_prime_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *PrimeDecompositionRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*PrimeDecompositionRequest) ProtoMessage() {}

func (x *PrimeDecompositionRequest) ProtoReflect() protoreflect.Message {
	mi := &file_proto_primepb_prime_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

func (*PrimeDecompositionRequest) Descriptor() ([]byte, []int) {
	return file_proto_primepb_prime_proto_rawDescGZIP(), []int{1}
}

func (x *PrimeDecompositionRequest) GetNumber() *Number {
	if x != nil {
		return x.Number
	}
	return nil
}

type PrimeDecompositionResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields
	Result int32 `protobuf:"varint,1,opt,name=result,proto3" json:"result,omitempty"`
}

func (x *PrimeDecompositionResponse) Reset() {
	*x = PrimeDecompositionResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_proto_primepb_prime_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *PrimeDecompositionResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*PrimeDecompositionResponse) ProtoMessage() {}
func (x *PrimeDecompositionResponse) ProtoReflect() protoreflect.Message {
	mi := &file_proto_primepb_prime_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

func (*PrimeDecompositionResponse) Descriptor() ([]byte, []int) {
	return file_proto_primepb_prime_proto_rawDescGZIP(), []int{2}
}

func (x *PrimeDecompositionResponse) GetResult() int32 {
	if x != nil {
		return x.Result
	}
	return 0
}
//ComputeAverage
type ComputeAverageRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields
	Number *Number `protobuf:"bytes,1,opt,name=number,proto3" json:"number,omitempty"`
}

func (x *ComputeAverageRequest) Reset() {
	*x = ComputeAverageRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_proto_primepb_prime_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ComputeAverageRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ComputeAverageRequest) ProtoMessage() {}
func (x *ComputeAverageRequest) ProtoReflect() protoreflect.Message {
	mi := &file_proto_primepb_prime_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

func (*ComputeAverageRequest) Descriptor() ([]byte, []int) {
	return file_proto_primepb_prime_proto_rawDescGZIP(), []int{3}
}
func (x *ComputeAverageRequest) GetNumber() *Number {
	if x != nil {
		return x.Number
	}
	return nil
}

type ComputeAverageResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Result float64 `protobuf:"fixed64,1,opt,name=result,proto3" json:"result,omitempty"`
}

func (x *ComputeAverageResponse) Reset() {
	*x = ComputeAverageResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_proto_primepb_prime_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ComputeAverageResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ComputeAverageResponse) ProtoMessage() {}
func (x *ComputeAverageResponse) ProtoReflect() protoreflect.Message {
	mi := &file_proto_primepb_prime_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

func (x *ComputeAverageResponse) GetResult() float64 {
	if x != nil {
		return x.Result
	}
	return 0
}
//grpc
type SumServiceClient interface {
	PrimeDecomposition(ctx context.Context, in *PrimeDecompositionRequest, opts ...grpc.CallOption) (SumService_PrimeDecompositionClient, error)
	ComputeAverage(ctx context.Context, opts ...grpc.CallOption) (SumService_ComputeAverageClient, error)
}

type sumServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewSumServiceClient(cc grpc.ClientConnInterface) SumServiceClient {
	return &sumServiceClient{cc}
}
func (c *sumServiceClient) PrimeDecomposition(ctx context.Context, in *PrimeDecompositionRequest, opts ...grpc.CallOption) (SumService_PrimeDecompositionClient, error) {
	stream, err := c.cc.NewStream(ctx, &SumService_ServiceDesc.Streams[0], "/primepb.SumService/PrimeDecomposition", opts...)
	if err != nil {
		return nil, err
	}
	x := &sumServicePrimeDecompositionClient{stream}
	if err := x.ClientStream.SendMsg(in); err != nil {
		return nil, err
	}
	if err := x.ClientStream.CloseSend(); err != nil {
		return nil, err
	}
	return x, nil
}

